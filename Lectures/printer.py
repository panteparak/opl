
sample = [7, [3, 2, [1, 4]], [5, [6, [2], 3]]]

def foo1(lst, depth=3):
  for itm in lst:
    if type(itm) is list:
      foo1(itm, depth + 3)
    else:
      print('-'*depth, "+", itm, sep="")


# foo1(sample)

total = 0
def foo2(lst):
  for itm in lst:
    if type(itm) is list:
      foo2(lst)
    else:
      global total
      total += itm

  global total
  print (total)

# foo2(sample)




