// def and(x: Boolean, y: Boolean) -> Boolean = if (!x) false else if (!y) false else true
// def or(x: Boolean, y: Boolean) -> Boolean = if (!)

def and(x: Boolean, y: => Boolean) = if (x) if (y) true else false else false
def or(x: Boolean, y: => Boolean) = if (x) true else if (y) true else false