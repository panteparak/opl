import itertools
class fib_sequence(object):
    def __init__(self):
        self.zero = 1
        self.one = 1
        self.i = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.i < 1:
            self.i += 1
            return 1
        else:
            tmp = self.zero + self.one
            self.zero = self.one
            self.one = tmp
            
            return self.one


fib = fib_sequence()
# print(list(itertools.islice(fib, 10)))
# print(list(itertools.takewhile(lambda f: f < 100, fib_sequence())))

def foo():
    yield 3
    yield 5
    yield 2

# list(foo())

def combine_generators(g1, g2):
    # while True:
    #     try:
    #         yield next(g1)
    #     except StopIteration as identifier:
    #         yield next(g2)
    yield from g1
    yield from g2

a = combine_generators(iter([1, 2, 3]), iter([4, 5, 6]))

for _ in range(6):
    print(next(a))