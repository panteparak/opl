class my_range(object):
  def __init__(self, n):
      self.n = n
      self.index = 0
    
  def __iter__(self):
      return self

  def __next__(self):
    if self.index < self.n:
        index = self.index
        self.index += 1
        return index
    else:
        raise StopIteration




for number in my_range(5):
  print(number)
