# Lecture 9 OPL

```scala
def findMin(xs: List[Int]): Option[Int] = xs match {
    case Nil => None
    case head::Nil => Some(head)
    case head::tail => Some(math.min(head, findMin(tail).get)
}
```

```scala
def nth(xs: List[Int], k: Int): Int = {
    
    @tailrec
    def tailnth(xs: List[Int], kth: Int, i: Int): Int = (i, xs) match {
        case (i, h::Nil) if i >= kth => tailnth(t, kth, i + 1)
        case _ => Nil
    }
}
```

```scala
sealed trait MyOptionInt
case object MyNone extends MyOptionInt
case Class MySome(value: Int) extends MyOptionInt

sealed trait MyOption[+T]
case object MyNone extends MyOption[Nothing]
case class MySome[T](value: T) extends MyOption[T]

def zip(xs: List[Int], ys: List[Int]): List[(Int, Int)] = (xs, ys) = match {
    case (Nil, Nil) => Nil
    case (xs::xs, y::ys) => (x, y)::zip(xs, ys)
    case _ = ??? // should never happen
}
```

```scala
def isPrime(n: Int) = (n > 1) && (1 to n).count(n  % x == 0) == 2
def countPrime(n: Int): Int = (1 to 1000).count(isPrime)
```

```scala
def topK(xs: List[Int], k: Int): List[Int] = {
    xs.map(f => (f,1)).reduceByKey{(a,b) => a + b}.key    
}

```

s