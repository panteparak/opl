class Reverse extends App {
  def decrList(n: Int): List[Int] = if (n == 0) List[Int](0) else n::decrList(n - 1)


  def decrListPattern(n: Int): List[Int] = n match {
    case 0 => List(0)
    case _ => n ::decrListPattern(n - 1)
  }
  println( decrList(5))



  def concat(xs: List[Int], ys: List[Int]): List[Int] = xs match {
    case Nil => Nil
    case head::tail => head :: concat(tail, ys)
  }
}
