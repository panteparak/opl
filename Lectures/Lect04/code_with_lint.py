"""
This file is linted
"""
def multiply(left: int, right: int) -> int:
    """
    This returns the result of a multiplation of the inputs
    """
    return left * right

def is_sum_lucky(left: int, right: int) -> str:
    """
    This returns a string describing whether or not the sum of input is lucky
    This function first makes sure the inputs are valid and then calculates the
    sum. Then, it will determine a message to return based on whether or not that
    sum should be considered "lucky"
    """
    result = left + right
    if result == 7:
        return 'a lucky number!'
    return 'an unlucky number!'

class SomeClass:
    """
    Some class
    """
    def __init__(self, someArg, someOtherArg):
        self.some_other_arg = someOtherArg
        self.some_arg = someArg

    def this_is_one(self):
        """
        One
        """
        pass

    def this_is_two(self):
        """
        Two
        """
        pass
