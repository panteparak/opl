def twice(functor):
  def func(*args):
    functor(*args)
    functor(*args)
  return func

def repeat(n=5):
    def real_decorator(function):
        def wrapper(*args, **kwargs):
            for _ in range(n):
                function(*args, **kwargs)
        return wrapper
    return real_decorator

@repeat
def say_hello(name):
    print(f'Hello, {name}')

# say_hello("Pan")