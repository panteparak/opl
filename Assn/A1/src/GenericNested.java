import javafx.util.Pair;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class GenericNested<E> implements NestedList<E>, Iterable<Pair<E, Integer>> {

    private E baseVal;
    private List<NestedList<E>> list;
    private boolean base;
    private BlockingQueue<Pair<E, Integer>> blockingQueue = null;
    private ThreadPoolExecutor executor = null;

    public GenericNested(E baseVal){
        this.baseVal = baseVal;
        this.base = true;
    }

    public GenericNested(List<NestedList<E>> lst){
        this.list = lst;
        this.base = false;
    }

    @Override
    public boolean isBase() {
        return base;
    }

    @Override
    public E getBaseValue() {
        if (!isBase()){
            throw new IllegalArgumentException("Not a base");
        }

        return baseVal;
    }

    @Override
    public List<NestedList<E>> getList() {
        if (isBase()){
            throw new IllegalArgumentException("Not a Base");
        }

        return list;
    }

    private synchronized ThreadPoolExecutor getExecutor(){
        if (executor == null){
            executor = new ThreadPoolExecutor( 1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>() );
        }

        return executor;
    }

    private synchronized BlockingQueue<Pair<E, Integer>> getBlockingQueue(){
        if (this.blockingQueue == null){
//            this.blockingQueue = new ArrayBlockingQueue<>(1);
            this.blockingQueue = new LinkedBlockingDeque<>(1);
        }

        return this.blockingQueue;
    }

    @Override
    public Iterator<Pair<E, Integer>> iterator() {
        if (this.isBase()){
            return new Iterator<Pair<E, Integer>>() {
                private boolean printed = false;

                @Override
                public boolean hasNext() {
                    return !printed;
                }

                @Override
                public Pair<E, Integer> next() {
                    printed = true;
                    return new Pair<>(getBaseValue(), 1);
                }
            };
        }

        if (getExecutor().getQueue().isEmpty()) {
            getExecutor().execute(new Job<>(getBlockingQueue(), getList()));
        }

        return new Iterator<Pair<E, Integer>>() {

            @Override
            public boolean hasNext() {
                boolean isThreadPoolEmpty = getExecutor().getQueue().isEmpty();
                boolean isEventBusEmpty = blockingQueue != null && blockingQueue.isEmpty();
//                System.out.printf("isThreadPoolEmpty: %s, isEventBusEmpty: %s\n", Boolean.toString(isThreadPoolEmpty), Boolean.toString(isEventBusEmpty));

                if  ((isThreadPoolEmpty && isEventBusEmpty)){
                    getExecutor().shutdown();
                    return false;
                }

                return true;
            }

            @Override
            public Pair<E, Integer> next() {
                return getBlockingQueue().poll();
            }

        };
    }

    private static class Job<E> implements Runnable {
        private BlockingQueue<Pair<E, Integer>> blockingQueue;
        private Stack<Pair<NestedList<E>, Integer>> stack;

        public Job(BlockingQueue<Pair<E, Integer>> blockingQueue, List<NestedList<E>> list){
            this.blockingQueue = blockingQueue;
            this.stack = new Stack<>();
            List<Pair<NestedList<E>, Integer>> items = list
                    .stream()
                    .map(item -> new Pair<>(item, 1))
                    .collect(Collectors.toList());

            this.stack.addAll(items);

        }

        @Override
        public void run(){
            while (!this.stack.isEmpty()) {
                Pair<NestedList<E>, Integer> node = this.stack.pop();
                NestedList<E> list = node.getKey();
                Integer depth = node.getValue();

                if (list.isBase()){
                    try {
                        this.blockingQueue.put(new Pair<>(list.getBaseValue(), depth));
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                } else {
                    for (NestedList<E> item : list.getList()){
                        this.stack.push(new Pair<>(item, depth + 1));
                    }
                }
            }


        }
    }
}
