import java.util.Arrays;
import java.util.Iterator;

public class Alternate<E> implements Iterable<E> {
    private final Iterator<E> lstLeft;
    private final Iterator<E> lstRight;

    public Alternate(Iterable<E> lstLeft, Iterable<E> lstRight) {
        this.lstLeft = lstRight.iterator();
        this.lstRight = lstLeft.iterator();
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private boolean isLeft;

            @Override
            public boolean hasNext() {
                return lstLeft.hasNext() || lstRight.hasNext();
            }

            @Override
            public E next() {
                if (isLeft){
                    right();
                    if (lstLeft.hasNext()){
                        return lstLeft.next();
                    }
                    return lstRight.next();

                } else {
                    left();
                    if (lstRight.hasNext()){
                        return lstRight.next();
                    }
                    return lstLeft.next();
                }
            }

            private void left(){
                this.isLeft = true;
            }

            private void right(){
                this.isLeft = false;
            }
        };
    }

    public static void main(String[] args) {
        Alternate<Integer> alternate = new Alternate<>(Arrays.asList(1, 2, 11, 12), Arrays.asList(4, 5, 6));
        for (Integer i : alternate){
            System.out.println(i);
        }
    }

}
