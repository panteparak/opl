"""stitcher streamer"""


def stitch_files(*paths):
    """stitch files and read content line-by-line"""
    for path in paths:

        if isinstance(path) != str:
            raise ValueError("Invalid parameter")

        with open(path, 'r') as read_stream:
            for line in read_stream:
                yield line
