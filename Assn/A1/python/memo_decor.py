"""Decorators with result caching"""

from math import factorial


def memoized(func):
    """Result caching decorator based on arg key"""
    memo = {}

    def wrapper(*args, **kwargs):
        if args in memo:
            return memo[args]

        ret = func(*args, **kwargs)
        memo[args] = ret
        return ret
    return wrapper


@memoized
def n_choose_r(number, coefficient):
    """nCr"""
    return factorial(number) / factorial(coefficient) / factorial(number - coefficient)
