"""LOL"""


def iter_dovetail(*iterators):
    """Iterators"""
    iter_count = len(iterators)
    error = [False for _ in range(iter_count)]
    position = 0
    while not is_all_true(error):
        for i in range(position, -1, -1):
            if error[i]:
                continue

            iterator = iterators[i]
            try:
                if not error[i]:
                    yield next(iterator)
            except StopIteration:
                error[i] = True
        position = (position + 1) % iter_count


def is_all_true(lst):
    """ Is all elements true"""
    return lst.count(True) == len(lst)


if __name__ == '__main__':
    for _ in iter_dovetail(iter([3, 5, 9]), iter([4]), iter([8, 7, 11, 6])):
        print(_)
