"""
permutations
"""
def all_perms(number):
    """
    compute all_perms
    """
    if number == 1:
        yield (1,)
    else:
        for perms in all_perms(number - 1):
            for index in range(number):
                yield perms[:index] + (number,) + perms[index:]


for i in all_perms(3):
    print(i)
