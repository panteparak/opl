import scala.annotation.tailrec

object Factorial extends App {
  def factorial(n: Int): Long = {
    @tailrec
    def tailFac(n: Int, accumulate: Long): Long = n match {
      case 0 | 1 => accumulate
      case _ => tailFac(n - 1, accumulate * n)
    }
    tailFac(n, 1)
  }
}
