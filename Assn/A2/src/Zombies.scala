object Zombies extends App {
  def countBad(hs: List[Int]): Int = {


    def mergeSort(xs: List[Int], zombies: Int): (List[Int], Int) = xs match {

      case a::Nil => (xs, zombies)
      case _ => {
        val (left, right) = xs.splitAt(xs.length / 2)
        val leftHalf = mergeSort(left, zombies)
        val rightHalf = mergeSort(right, zombies)

        merge(leftHalf._1, rightHalf._1, leftHalf._2 + rightHalf._2)
      }
    }

    def merge(left: List[Int], right: List[Int], zombies: Int): (List[Int], Int) = (left, right) match {
      case (l, Nil) => (l, zombies)
      case (Nil, r) => (r, zombies)
      case (lh::lt, rh::rt) =>
        if (lh < rh) {
          val (ll, rr) = merge(lt, right, zombies + right.length)
          (lh::ll, rr)
        } else {
          val (ll, rr) = merge(left, rt, zombies)
          (rh :: ll, rr)
        }
    }
    mergeSort(hs, 0)._2
  }

  println(countBad(List(5,4,11,7)))
  println(countBad(List(3, 1, 4, 2)))
  println(countBad(List(1, 7, 22, 13, 25, 4, 10, 34, 16, 28, 19, 31)))
}