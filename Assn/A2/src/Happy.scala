import scala.annotation.tailrec

object Happy extends App {
  def sumOfDigitsSquared(n: Int): Int = {

    @tailrec
    def tailSumDigitSqrt(n: Int, sumSquare: Int): Int = n match {
      case 0 => sumSquare
      case _ => {
        val lastDigit = n % 10
        tailSumDigitSqrt(n / 10, (lastDigit * lastDigit) + sumSquare)
      }
    }

    tailSumDigitSqrt(n, 0)
  }

  def isHappy(n: Int): Boolean = {

    @tailrec
    def tailHappy(n: Int): Boolean = n match {
      case 1 => true
      case 4 => false
      case _ => tailHappy(sumOfDigitsSquared(n))
    }

    tailHappy(n)
  }

  def kThHappy(k: Int): Int = {

    @tailrec
    def tailK(k: Int, i: Int, result: Int): Int = k match {
      case 0 => result
      case _ => {
          isHappy(i) match {
          case true => tailK(k - 1, i + 1, i)
          case false => tailK(k, i + 1, result)
        }
      }
    }

    tailK(k, 1, 0)
  }
}
