object Aggregate extends App {
  // todo: implement these functions for real!
  def myMin(p: Double, q: Double, r: Double): Double = if (p < q && p < r) p else if (q < p && q < r) q else r
  def myMean(p: Double, q: Double, r: Double): Double = (p + q + r) / 3
  def myMed(p: Double, q: Double, r: Double): Double = if (p <= q && q <= r) q else if (q <= r && r <= p) r else p
}
