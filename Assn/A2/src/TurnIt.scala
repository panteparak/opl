import scala.annotation.tailrec

object TurnIt extends App {

  val matrixOne = List(
    List(1,2,3),
    List(4,5,6),
    List(7,8,9)
  )
  val matrixTwo = List(
    List(1,2,3),
    List(4,5,6),
    List(7,8,9),
    List(10,11,12)
  )
  //  1 4 7
  //  2 5 8
  //  3 6 9
  val matrixThree = List(
    List(1, 2),
    List(3, 4),
    List(5, 6),
    List(7, 8)
  )

  //  1 4 7 10
  //  2 5 8 11
  //  3 6 9 12
  val empty = List()

  def transpose(A: List[List[Int]]): List[List[Int]] = {

    def mergeCol(A: List[List[Int]], B: List[List[Int]]): List[List[Int]] = (A, B) match {
      case (Nil,Nil) => Nil
      case ((aHead::_)::aTail, bHead::bTail) => (aHead::bHead)::mergeCol(aTail, bTail)
    }

    @tailrec
    def mutate(A: List[Int], ret: List[List[Int]]): List[List[Int]] = A match {
      case Nil => ret
      case h::t => mutate(t, List(h)::ret)
    }

    @tailrec
    def tailTranspose(A: List[List[Int]], colMutated: List[List[Int]], ret: List[List[Int]]):List[List[Int]] = (A, colMutated, ret) match {
      case (Nil, Nil, _) => ret
      case (aHead::aTail, Nil, Nil) => tailTranspose(aTail, Nil, mutate(aHead, List()))
      case (aHead::aTail, Nil, out) => tailTranspose(aTail, Nil, mergeCol(mutate(aHead, List()), out))
      case (Nil, m, out) => tailTranspose(Nil, Nil, mergeCol(m, out))
    }

    @tailrec
    def reverseTail(A: List[List[Int]], ret: List[List[Int]]): List[List[Int]] = A match {
      case Nil => ret
      case h::t => reverseTail(t, h.reverse::ret)
    }

    reverseTail(tailTranspose(A, List(), List()), List())
  }
}
